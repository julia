(in-package :julia)

(defmacro defdispatch (name &body body)
  `(defun ,(intern (format nil "DISPATCH-~s" name)) (from to uid args)
     (declare (ignorable from to uid args))
     (logger "dispatch ~s: from=~a to=~a uid=~a args='~a'~%" ',name from to uid args)
     ,@body))

(defdispatch who
  (send-msg from 'know (format nil "~a" *ids*)))

(defdispatch hello
  (pushnew from *ids* :test #'string=)
  (setq *ids* (sort *ids* #'string<))
  (coordinate)
  (logger "new *ids*: ~a~%" *ids*))

(defdispatch bye
  (setq *ids* (sort (delete from *ids* :test #'string=) #'string<))
  (coordinate)
  (logger "new *ids*: ~a~%" *ids*))

;; FIXME race condition if new WRITEs/COORDINATORs appear between syncs
(defdispatch sync
  (let ((last-gen (parse-integer args)))
    (logger "last-gen ~a~%" last-gen)
    (loop for (id . data) in (reverse *feeds*)
	 when (>= id last-gen) do
	 (send-msg from 'feed (format nil "~a ~a" id data)))))

(defdispatch feed
  (let (id data p)
    (setq p (search " " args)
	  id (parse-integer (subseq args 0 p))
	  data (subseq args (1+ p)))
    (assert (= id *gen*))
    (feed data)))

(defdispatch know
  (setq *ids*
	(loop with new = (read-from-string args)
	   for i in new
	   when (string/= i *me*) do
	     (pushnew i *ids* :test #'string=)
	   finally (return (sort *ids* #'string<))))
  (logger "new *ids*: ~a~%" *ids*)
  (coordinate)
  (when *not-synced*
    (setq *not-synced* nil)
    (send-msg from 'sync *gen*)))

(defdispatch write
;; FIXME add support for critical sections
  (when (and *coordinator* (string= to "COORDINATOR"))
    (send-msg :all 'write args)
    (send-msg from 'ack (format nil "~a" uid)))
  (feed args))

#+nil(defdispatch read
  (let ((key (sxhash (cons *gen* args))))
    (multiple-value-bind (result presentp)
	(gethash key *cache*)
      (send-msg from 'answer
		(format nil "~d ~d ~a" uid *gen*
			(if presentp
			    result
			    (let ((ret (rep args)))
			      (logger "eval~%")
			      (setf (gethash key *cache*)ret)
			      ret)))))))

(defdispatch read
    (send-msg from 'answer
	      (format nil "~d ~d ~a" uid *gen*
		      (let ((ret (rep args)))
			(logger "eval~%")
			ret))))

;
