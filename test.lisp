(asdf:oos 'asdf:load-op :zeromq)

(defpackage :julia-test
  (:use :cl))

(in-package :julia-test)

(defvar *bus* "epgm://lo;226.0.0.1:5555")
(defvar *ctx*)
(defvar *bus-in*)
(defvar *bus-out*)
(defvar *poll-in*)
(defvar *me* "TROLL")
(defvar *uid* 100)

(defun send-msg (to cmd args)
  (let ((data (format nil "~a ~a ~a ~a ~a" to *me* (incf *uid*) cmd args)))
    (format t "send-msg> ~s~%" data)
    (zmq:send *bus-out* (make-instance 'zmq:msg :data data))))

(defun recv-msg (&optional (timeout -1))
  (let ((ret (zmq:poll *poll-in* timeout))
	(msg (make-instance 'zmq:msg))
	rep)
    (when (not (null ret))
      (zmq:recv *bus-in* msg)
      (setq rep (zmq:msg-data-as-string msg))
      (format t "recv-msg> ~s~%" rep)
      rep)))

(defun init ()
  (setq *ctx* (zmq:init 1)
	*bus-in* (zmq:socket *ctx* zmq:sub)
	*bus-out* (zmq:socket *ctx* zmq:pub)
	*poll-in* (list (make-instance 'zmq:pollitem
				       :socket *bus-in* :events zmq:pollin)))
  (zmq:setsockopt *bus-in* zmq:subscribe *me*)
  (zmq:connect *bus-in* *bus*)
  (zmq:connect *bus-out* *bus*))

(defun shutdown ()
  (zmq:close *bus-in*)
  (zmq:close *bus-out*)
  (zmq:term *ctx*))

(init)

;(send-msg :coordinator 'write "(defvar my-list 1)")
;(send-msg :all 'write "(defvar my-list 1)")
;(send-msg :all 'write "(progn (incf my-list) (foo))")
;(send-msg :all 'read "my-list")
;(format t" > ~a~%" (recv-msg))

(send-msg :coordinator 'write "(defvar my-list 1)")
(format t" > ~a~%" (recv-msg))

(shutdown)
(sb-ext:quit)

;
