(asdf:oos 'asdf:load-op :julia)

(zmq::with-context (ctx 1)
  (zmq:with-socket (s ctx zmq:pub)
    (zmq:connect s julia::*bus*)
    (zmq:send s
	      (make-instance 'zmq:msg :data (format nil "ALL ~a 0 BYE" julia::*me*)))
    (sleep 1)))

(sb-ext:quit)

;
