;; Copyright (c) 2010 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is part of Julia.
;;
;; Julia is free software; you can redistribute it and/or modify it under
;; the terms of the Lesser GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; 0MQ is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; Lesser GNU General Public License for more details.
;;
;; You should have received a copy of the Lesser GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(cl:eval-when (:load-toplevel :execute)
  (asdf:operate 'asdf:load-op :zeromq)
  (asdf:operate 'asdf:load-op :local-time))

(defpackage #:julia-asd
  (:use :cl :asdf))

(in-package #:julia-asd)

(defsystem julia
  :name "julia"
  :version "0.1"
  :author "Vitaly Mayatskikh <v.mayatskih@gmail.com>"
  :licence "LGPLv3"
  :description "Julia distributed whatever"
  :serial t
  :components ((:file "package")
               (:file "utils")
	       (:file "dispatchers")
               (:file "julia")))
