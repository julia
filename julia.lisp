(in-package :julia)

(defun run-timer ()
  (sb-ext:schedule-timer *timer* 60))

(defun init ()
  (logger "Starting...~%")
  (init-messaging)

  (setq *timer* (sb-ext:make-timer 'julia::timer-fn)
	*cache* (make-hash-table))
  (push *me* *ids*)
  (coordinate)
  (run-timer)

;; Self-announce
  (send-msg :all 'hello))

(defun shutdown ()
  (send-msg :all 'bye)
  (shutdown-messaging))

(defun save-core ()
  (logger "Saving core...~%")
  (setq *not-synced* t
	*save-core* nil
	*last-gen* *gen*)

  ;; Wait for previous dump to finish (if it is still running)
  (when (plusp *save-pid*)
    (sb-posix:waitpid *save-pid* 0)
    (setq *save-pid* 0))

  (setq *save-pid* (sb-posix:fork))
  (cond
    ((zerop *save-pid*)
     (setq *poll-in* nil)		; pollitem is a bad guy
     (sb-ext:gc :full t)		; run finalizers
     (sb-ext:save-lisp-and-die (concatenate 'string "CORE-" *me*)
			       :toplevel #'julia::julia))
    ((minusp *save-pid*)
     (error "fork failed~%"))))

(defun timer-fn ()
  (when (< *last-gen* *gen*)
    (setq *save-core* t))
  (run-timer))

(defun feed (args)
  (when (safe-eval args)		; don't trash log
    (push (cons *gen* args) *feeds*)
    (setq *cache* (make-hash-table))
    (incf *gen*)))

(defun dispatch (cmd from to uid args)
  (let ((f (intern (concatenate 'string "DISPATCH-" (string-upcase cmd)))))
    (if (fboundp f)
	(funcall f from to uid args)
	(unintern f))))

(defun julia ()
  (in-package :julia)
  (init)

  ;; Who's there?
  (send-msg :all 'who)

  (handler-case
      (loop
	 (progn
	   (when *save-core*
	     (save-core))
	   (multiple-value-bind (to from uid cmd args)
	       (hdr (recv-msg))
	     (when (string/= from *me*)
	       (dispatch cmd from to uid args)))))
    (error (c) (logger "BUG: ~a~%" c)))

  (shutdown))

(defun prepare ()
  (save-core)
  (sb-ext:quit))
;
