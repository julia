(in-package :julia)

(defvar *bus* "epgm://lo;226.0.0.1:5555")

(defvar *ctx* nil
  "0MQ context")
(defvar *bus-in* nil
  "Input bus")
(defvar *bus-out* nil
  "Output bus")
(defvar *poll-in* nil
  "Pollitem")
(defvar *me* (sb-unix::posix-getenv "NODE")
  "Our identifier")
(defvar *uid* 0
  "Sequental send-msg counter")
(defvar *ids* nil
  "Nodes present at bus")
(defvar *gen* 0
  "Generation ID")
(defvar *last-gen* 0
  "Last saved generation ID")
(defvar *feeds* nil
  "Data propagation database")
(defvar *cache* nil
  "Cache for replies")
(defvar *not-synced* t
  "Ask neighbours for missed updates.")
(defvar *save-core* nil
  "Flag: save core when possible.")
(defvar *save-pid* 0)
(defvar *timer* nil)
(defvar *coordinator* nil
  "`t' if coordinator.")

(defun logger (&rest args)
  (declare (ignorable args))
  (format t "~a ~a> " (local-time:now) *me*)
  (apply #'format t args))

(defun init-messaging ()
  (setq *ctx* (zmq:init 1)
	*bus-in* (zmq:socket *ctx* zmq:sub)
	*bus-out* (zmq:socket *ctx* zmq:pub)
	*poll-in* (list (make-instance 'zmq:pollitem
				       :socket *bus-in* :events zmq:pollin)))

;; Subscribe to common bus
  (zmq:setsockopt *bus-in* zmq:subscribe "ALL")
  (zmq:connect *bus-in* *bus*)

;; Subscribe to our private bus
  (zmq:setsockopt *bus-in* zmq:subscribe *me*)
  (zmq:connect *bus-out* *bus*))

(defun shutdown-messaging ()
  (zmq:close *bus-in*)
  (zmq:close *bus-out*)
  (zmq:term *ctx*))

(defun send-msg (to cmd &optional args)
  (let ((data (format nil "~a ~a ~a ~a ~a" to *me* (incf *uid*) cmd (or args ""))))
    (logger "[send-msg] ~s~%" data)
    (zmq:send *bus-out* (make-instance 'zmq:msg :data data))))

(defun recv-msg (&optional (timeout -1))
  (let ((ret (zmq:poll *poll-in* timeout))
	(msg (make-instance 'zmq:msg))
	rep)
    (when (not (null ret))
      (zmq:recv *bus-in* msg)
      (setq rep (zmq:msg-data-as-string msg))
      (logger "[recv-msg] ~s~%" rep))
    rep))

(defun hdr (msg)
  "Splits string into parts."
  (let (from to uid cmd args p1 p2)
    (handler-case
	 (setq p1 (search " " msg)
	       to (subseq msg 0 p1)
	       p1 (1+ p1)
	       p2 (search " " msg :start2 p1)
	       from (subseq msg p1 p2)
	       p2 (1+ p2)
	       p1 (search " " msg :start2 p2)
	       uid (subseq msg p2 p1)
	       p1 (1+ p1)
	       p2 (search " " msg :start2 p1)
	       cmd (subseq msg p1 p2)
	       args (or (and p1 (subseq msg (1+ p2))) ""))
      (error ()))
    (values to from uid cmd args)))

(defun rep (expr)
  (eval (read-from-string expr)))

(defun safe-eval (expr)
  (logger "safe eval: ~a~%" expr)
  (let ((pid (sb-posix:fork)))
    (cond
      ((plusp pid)
       (multiple-value-bind (pid status)
	   (sb-posix:waitpid pid 0)
	 (declare (ignore pid))
	 (if (zerop (ash status -8))
	     (progn (rep expr) t)
	     (progn (logger "safe eval failed~%") nil))))
      ((zerop pid)
       (handler-case
	   (rep expr)
	 (error ()
	   (sb-ext:quit :unix-status 1)))
       (sb-ext:quit :unix-status 0))
      (t
       (progn (error "fork failed~%") nil)))))

(defun coordinate ()
  (cond
    ((and (not *coordinator*)
	  (string= (car *ids*) *me*))
;; FIXME race condition
;; If there's real coordinator, but we don't know about it yet and
;; receive new COORDINATOR messages.
     (unwind-protect
	  (zmq:setsockopt *bus-in* zmq:subscribe "COORDINATOR"))
     (setq *coordinator* t)
     (logger "I'm the coordinator~%"))
    ((and *coordinator*
	  (string/= (car *ids*) *me*))
     (unwind-protect
	  (zmq:setsockopt *bus-in* zmq:unsubscribe "COORDINATOR"))
;; FIXME race condition
;; Contrary problem
     (setq *coordinator* nil)
     (logger "New coordinator is ~a~%" (car *ids*)))))

;
